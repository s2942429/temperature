package nl.utwente.di.tempConvert;

public class Quoter {
    public double convertTemp(String celsiusValueString) {
        double celsiusValue = Double.parseDouble(celsiusValueString);
        return (celsiusValue * 9/5) + 32;
    }
}
